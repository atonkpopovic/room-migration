# Room migration

A sample application to present Android Room migration without losing existing data.  

Usacase:

- switch to the room_v1 branch and generate .apk
- install the .apk to a device
- create few records 

- switch to the room_v2 branch and generate .apk
- update the existing application
- create few records with new features

Expected behavior:

- All records from v1 and v2 will be displayed  